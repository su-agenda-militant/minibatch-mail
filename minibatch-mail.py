# /usr/bin/env python3

import argparse
import configparser
import smtplib
import ssl
from email.message import EmailMessage
import time
from os.path import basename
import mimetypes
import csv

try:
    from tqdm import tqdm
except:
    def tqdm(x):
        return x

def load_addresses(path):
    with open(path) as fp:
        for line in fp:
            line = line.rstrip("\n")
            for addr in line.split(","):
                addr = addr.strip()
                if len(addr) == 0:
                    continue
                yield addr

def load_csv(path, column):
    with open(path) as fp:
        reader = csv.reader(fp, delimiter="\t")
        for row in reader:
            addr = row[-1]
            if len(addr) > 0:
                yield row[column]

def minibatch(data, size):
    fullsize = len(data)
    remaining = fullsize % size
    for begin in range(0, fullsize, size):
        yield data[begin:begin+size]

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Minibatch-mail')
    parser.add_argument("--config", default="minibatch-mail.conf")
    parser.add_argument("--to", default=None, help="Text file with addresses (comma or newlines) or CSV files (see --column)")
    parser.add_argument("--column", default=None, help="Column number for emails in CSV file")
    parser.add_argument("--html", default=None, help="HTML part for a multipart/alternative message")
    parser.add_argument("-a", "--attach", default=[], action='append', help="Attach a file")
    parser.add_argument("--dummy", action="store_true", help="No operation")
    parser.add_argument("subject")
    parser.add_argument("content")
    args = parser.parse_args()

    config = configparser.ConfigParser()
    config.read(args.config)

    if args.column is None:
        addresses = list(load_addresses(args.to))
    else:
        addresses = list(load_csv(args.to, int(args.column)))
    print("Found", len(addresses), "addresses")

    smtp = smtplib.SMTP(config["smtp"]["server"], config["smtp"]["port"])
    smtp.starttls(context=ssl.create_default_context())
    smtp.login(config["smtp"]["user"], config["smtp"]["password"])

    msg = EmailMessage()
    msg["To"] = "" # dummy
    msg["Subject"] = args.subject
    msg["From"] = config["message"]["From"]
    msg["Cc"] = config["message"]["Cc"]
    msg["Bcc"] = config["message"]["Bcc"]

    with open(args.content) as fp:
        msg.set_content(fp.read())

    if args.html is not None:
        with open(args.html) as fp:
            msg.add_alternative(fp.read(), subtype='html')

    for path in args.attach:
        ctype, encoding = mimetypes.guess_type(path)
        if ctype is None or encoding is not None:
            # No guess could be made, or the file is encoded (compressed), so
            # use a generic bag-of-bits type.
            ctype = 'application/octet-stream'
        maintype, subtype = ctype.split('/', 1)
        with open(path, 'rb') as fp:
            data = fp.read()
            msg.add_attachment(data,
                               maintype=maintype,
                               subtype=subtype,
                               filename=basename(path))

    addresses = list(minibatch(addresses, int(config["smtp"]["minibatch"])))
    for dest in tqdm(addresses):
        if len(dest) == 1:
            msg.replace_header("To", dest)
        else:
            bcc = dest
            if config["message"]["Bcc"] != "":
                bcc.append(config["message"]["Bcc"])
            msg.replace_header("Bcc", bcc)
        if not args.dummy:
            smtp.send_message(msg, from_addr=config["smtp"]["from"])
            time.sleep(int(config["smtp"]["delay"]))
        else:
            # print("Dummy", dest)
            # time.sleep(int(config["smtp"]["delay"]))
            # print(msg)
            # xxx
            pass

